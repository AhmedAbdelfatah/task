var app = angular.module("AdminApp", ["moment-picker", "ngRoute"]);

app.run(function($rootScope) {
  $rootScope.account = false;
  $rootScope.server = "../API/index.php/";

  $rootScope.check = function() {
    if (!$rootScope.account) $rootScope.href("init");
  };

  $rootScope.href = function(a) {
    location.href = angular.isArray(a) ? "#/" + a.join("/") : "#/" + a;
  };
});
