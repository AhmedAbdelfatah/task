<?php
class Students extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			header('Access-Control-Allow-Origin: *');
			$this->load->model('utils/Utils');
			$this->load->model('db/Students_model','Students');
			$this->admin=$this->Utils->admin_token();
			$this->lists=['status'=>array(1=>'Active',2=>'Inactive')];
    }
    public function index()
	{
		$rows=$this->Students->rows();
		$rows['lists']=$this->lists ;
		$this->Utils->Result( 	$rows	 );
	}
	
	public function view($id=0)
	{
		$data=$this->Students->row($id);

		if($id<>0){
			$data->date_of_birth= date('Y-m-d', strtotime($data->date_of_birth));
		}
		$res=array('row'=>$data);
		$res['lists']=$this->lists;
		$this->Utils->Result( 	$res	 );
	}
	public function add()
	{
		$data=$this->input->post('data');
		unset($data['id']);
		if(!$data['first_name']) $this->Utils->Result(0,1,'ERROR_NAME');
		if(!$data['last_name']) $this->Utils->Result(0,1,'ERROR_NAME');
		if(!$data['date_of_birth']) {
			$this->Utils->Result(0,1,'ERROR_NAME');
		}else{
			$var = $data['date_of_birth'];
            $date = str_replace('/', '-', $var);
			$data['date_of_birth']= date('Y-m-d', strtotime($date));
		};
		$res=$this->Students->insert($data);
		$this->Utils->Result(array('insert_id'=>$res),$res==0);
	}
	public function edit($id)
	{
		$data=$this->input->post('data');
		unset($data['id']);
		$var = $data['date_of_birth'];
		$date = str_replace('/', '-', $var);
		$data['date_of_birth']= date('Y-m-d', strtotime($date));
		$res=$this->Students->update($data,array('id'=>intval($id)));
		$this->Utils->Result(array('affected'=>$res),$res==0);
	}
	 
	
}	

